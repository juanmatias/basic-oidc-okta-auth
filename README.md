# Simple Login with OIDC Okta

Once you master this, you can switch to a provider other than Okta.

## Set up

Create a virtual env:

``` shell
# Clone this repo and CD into it
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

In Okta create an OIDC app for Web.
Set "Sign-in redirect URIs" to "http://localhost:5000/callback" and "Sign-out redirect URIs" to "http://localhost:5000".
Assign the user you want to use in this test to the app.
Get the Client ID and secret.

Set the env vars. Create a file .env like this:

``` shell
export CLIENTID="theclientid"
export CLIENTSECRET="theclientsecret"
export BASEURL="theOktabaseurl"
```

Note: The base url is something like "yourorgname.okta.com".

## Run the example

Just run it:

``` shell
source .env && python main.py
```

Now hit this URL in your browser: [http://localhost:5000](http://localhost:5000)

Enhoy the OIDC magic.

## A little bit of explanation

There is a function (or endpoint) called "/login", when the user hits it is redirected to the /authorize endpoint in Okta.
This redirection is made sending the client id and the redirect URI (the address the user will be redirected to from Okta after the authentication/authorization process is done).

There is a function called "/callback". The user will be redirected here after the auth process in Okta. This redirection is made with a parameter called "code".

This code is a short-lived code that your site has to exchange for the actual ID and Access Tokens. This process is done in the same callback function.

Finally this example stores the access token in the session. But actually, you have to decide what to do from here. At this point the user is authenticated and authorized in your app.
Now you can do a myriad of things, e.g.:
  * create your own session with a token
  * check the access token using the Okta endpoint called "/introspect"
  * store the access token in a redis to keep it safe while the session is live

But this will depend on what your app does, what's your security schema, etc.

Enjoy!
