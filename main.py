#!/usr/bin/env python3

from flask import Flask, request, redirect, session
import httpx
import json
import os

appFlask = Flask(__name__)
appFlask.secret_key = "tu vieja"

if not 'CLIENTID' in os.environ or not 'CLIENTSECRET' in os.environ or not 'BASEURL' in os.environ:
    raise Exception('No env vars detected')

_client_id = os.environ['CLIENTID']
_client_secret = os.environ['CLIENTSECRET']
base_url = os.environ['BASEURL']

redirect_uri = 'http://localhost:5000/callback'
oauth_path = '/oauth2/v1'
protocol = 'https://'
@appFlask.route('/')
def index():
    if "access_token" in session:
        return '<h1>The groso site, user logged in!</h1><br /><a href="/logout">Logout</a>'
    else:
        return '<h1>The groso site, user unknow!</h1><br /><a href="/login2">Login</a>'

@appFlask.route('/login2')
def login():
    url = f"{protocol}{base_url}{oauth_path}/authorize?client_id={_client_id}&response_type=code&scope=openid&redirect_uri=http%3A%2F%2Flocalhost%3A5000%2Fcallback&state=tuviejaentanga&nonce=QG4e_o8Xco0cObAp5uxJ4pM53c"
    return redirect(url)

@appFlask.route('/callback')
def callback():
    code = request.args.get('code')
    error = request.args.get('error_description')

    if not error is None:
        return f"error: {error}", 400

    headers = {
        'accept': 'application/json',
        'cache-control': 'no-cache',
        'content-type': 'application/x-www-form-urlencoded'
        }
    data = {
        'client_id': _client_id,
        'client_secret': _client_secret,
        'grant_type': 'authorization_code',
        'redirect_uri': redirect_uri,
        'code': code
        }
    url = f"{protocol}{base_url}{oauth_path}/token"

    response = httpx.post(url, headers=headers, data=data)

    access_token = response.json()['access_token']
    session['access_token'] = access_token

    if response.status_code == httpx.codes.OK:
        return redirect('/')
    else:
        return f"error: {response.text}", 400

@appFlask.route('/logout')
def logout():
    session.pop('access_token', None)
    return redirect('/')

appFlask.run(debug=True, port=5000)
